package calendar

import (
	"github.com/fatih/color"
	"github.com/olekukonko/tablewriter"
	"os"
	"strconv"
	"time"
)

type MonthConf struct {
	Time time.Time
	Data map[int][]string
}

func RenderMonth(monthConf MonthConf) {
	table := tablewriter.NewWriter(os.Stdout)
	table.SetRowLine(true)
	table.SetHeader([]string{"Sunday", "Monday", "Tuesday", "Wednesday", "Thursdays", "Friday", "Saturday"})

	month := monthConf.Time.Month()
	workingDate := time.Date(
		monthConf.Time.Year(),
		month,
		1,
		monthConf.Time.Hour(),
		monthConf.Time.Minute(),
		monthConf.Time.Second(),
		monthConf.Time.Nanosecond(),
		monthConf.Time.Location())

	data := make([]string, 7)
	for {
		day := strconv.Itoa(workingDate.Day())

		if workingDate.Equal(monthConf.Time) {
			day = color.HiWhiteString(day)
		}

		text := day
		if events, ok := monthConf.Data[workingDate.Day()]; ok {
			for _, event := range events {
				text += "\n--" + event
			}
		}

		data[workingDate.Weekday()] = text

		if workingDate.Weekday() == time.Saturday {
			table.Append(data)
			data = make([]string, 7)
		}

		workingDate = workingDate.AddDate(0, 0, 1)
		if workingDate.Month() != month {
			table.Append(data)
			break
		}
	}

	table.Render()
}
