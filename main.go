package main

import (
	"github.com/spf13/cobra"
	"gocal/calendar"
	"time"
)

var rootCmd = &cobra.Command{
	Use:   "gocal",
	Short: "A fancy terminal based calendar application",
	Run:   runRoot,
}

const DateFormat = "2.1.2006"

var date string

func init() {
	rootCmd.PersistentFlags().StringVarP(&date, "date", "d", time.Now().Format(DateFormat), "tbd")
}

func main() {
	rootCmd.Execute()
}

func runRoot(_ *cobra.Command, _ []string) {
	t, err := time.Parse(DateFormat, date)
	if err != nil {
		panic(err)
	}

	monthConf := calendar.MonthConf{
		Time: t,
		Data: map[int][]string{
			5:                {"Ein Meeting", "Ein Folge-Meeting"},
			8:                {"Ein Meeting", "Ein Folge-Meeting"},
			16:               {"Ein Meeting", "Ein Folge-Meeting"},
			time.Now().Day(): {"Daily", "Qudomeeting"},
		},
	}

	calendar.RenderMonth(monthConf)
}
