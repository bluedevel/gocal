module gocal

require (
	github.com/fatih/color v1.7.0
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/mattn/go-colorable v0.0.9 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/mattn/go-runewidth v0.0.3 // indirect
	github.com/mitchellh/go-homedir v1.0.0 // indirect
	github.com/olekukonko/tablewriter v0.0.0-20180912035003-be2c049b30cc
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.2.0 // indirect
	golang.org/x/sys v0.0.0-20180918153733-ee1b12c67af4 // indirect
)
